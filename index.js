const express = require("express");
const morgan = require('morgan')
const app = express()
const port = process.env.PORT || 3000;

const { Game } = require("./models");
const { Biodata } = require("./models");
const { History } = require("./models");

app.use(express.json());
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(express.static('views'))
app.use('/css',express.static(__dirname + 'public/css'))
app.use('/js',express.static(__dirname + 'public/js'))
app.use('/img',express.static(__dirname + 'public/img'))
app.use('/img',express.static(__dirname + 'public/assets'))
app.use(express.urlencoded({
  extended: false,
}))
app.use(morgan('dev'))


app.post("/login", (req, res) => {
    const {username, password} = req.body;
    if(username == "admin" && password == "admin"){
      res.redirect("/home");
    }
    else{
      res.redirect("/");
    }
  });
  
//Router Home page
app.get("/", (req, res) => {
    res.render('login');
}) 
app.get("/home", (req, res) => {
    res.render('index');
}) 

//route create User
app.get("/game/create", (req, res) => {
  res.render('game/create');
}) 

app.get("/biodata/create", (req, res) => {
    res.render('biodata/create');
  }) 
//routing create history
app.get("/history/create", (req, res) => {
    res.render('history/create');
  })
  
app.get("/biodata/del", (req, res) => {
    res.render('biodata/delete');
  })


//---------------- End Route ----------------//


//Find All Data user game
app.get('/game', (req,res) => {
    Game.findAll().then((game) => {
        res.render('game/index', {
            game,
        })
    }).catch((err) => {
        res.status(422).json("Can't Find Data")
    });
})

//Post Articles
app.post('/game', (req, res) => {
    Game.create({
        username: req.body.username,
        password: req.body.password,
       
    }).then((game) => {
        Game.findAll().then((game) => {
            res.render('game', {
                game,
            })
        })
    }).catch((err) => {
        res.status(422).json("Can't Post Data")
    });
})


//Delete Biodata
app.get("/game_delete/(:id)", (req, res) => {
    Game.destroy({
        where: { id: req.params.id }
    }).then((game) => {
        Game.findAll().then((game) => {
            res.render('game', {
                game,
            })
        })
    }).catch((err) => {
        res.status(422).json("Can't Delete Data")
    });
});

//Get Data to Update
app.get("/game_update/(:id)", (req, res) => {
    Game.findOne({
        where: { id: req.params.id }
    }).then((game) => {
        res.render('game/update',{ 
            game
        })
    }).catch((err) => {
        res.status(422).json("Can't Get Data")
    });
});
//Update Biodata
app.post("/game-update/(:id)", (req,res) => {
    Game.update({
        username: req.body.username,
        password: req.body.password,
    },
    {
        where: { id: req.params.id},
    }
    ).then((game) => {
        Game.findAll().then((game) => {
            res.render('game', {
                game,
            })
        })
    }).catch((err) => {
        res.status(422).json("Can't Update Data")
    });
});

//------------------------------------------------//

//Post Articles
app.post('/biodata', (req, res) => {
    Biodata.create({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        city: req.body.city,
        country: req.body.country,
        telp: req.body.telp,
        instagram: req.body.instagram,
    }).then((biodata) => {
        Biodata.findAll().then((biodata) => {
            res.render('biodata/index', {
                biodata,
            })
        })
    }).catch((err) => {
        res.status(404).json("Record not found")
    });
})

//FindAll Biodata
app.get('/biodata', (req,res) => {
    Biodata.findAll().then((biodata) => {
        res.render('biodata/index', {
            biodata,
        })
    }).catch((err) => {
        res.status(404).json("Record not found")
    });
})

//Delete Biodata
app.get("/biodata_delete/(:id)", (req, res) => {
    Biodata.destroy({
        where: { id: req.params.id }
    }).then((biodata) => {
        
        Biodata.findAll().then((biodata) => {
            res.render('biodata', {
                biodata,
            })
        })
    }).catch((err) => {
        res.status(422).json("Can't Delete Data")
    });
});

//Get Data to Update
app.get("/biodata_update/(:id)", (req, res) => {
    Biodata.findOne({
        where: { id: req.params.id }
    }).then((biodata) => {
        res.render('biodata/update',{ 
            biodata
        })
    }).catch((err) => {
        res.status(422).json("Can't Get Data")
    });
});

//Update Biodata
app.post("/biodata-update/:id", (req,res) => {
    Biodata.update({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        telp: req.body.telp,
        city: req.body.city,
        country: req.body.country,
        instagram: req.body.instagram,
    },
    {
        where: { id: req.params.id},
    }
    ).then((biodata) => {
        Biodata.findAll().then((biodata) => {
            res.render('biodata', {
                biodata,
            })
        })
    }).catch((err) => {
        res.status(422).json("Can't Update Data")
    });
});


//------------------------------------------------//

//Find All Data user game
app.get('/history', (req,res) => {
    History.findAll().then((history) => {
        res.render('history/index', {
            history,
        })
    })
})
//History Post
app.post('/history', (req, res) => {
    History.create({
        username: req.body.username,
        score: req.body.score,
        time: req.body.time,
        round: req.body.round,
        level: req.body.level,
       
    }).then((history) => {
        History.findAll().then((history) => {
            res.render('history/create', {
                history,
            })
        }).catch((err) => {
            res.status(422).json("Can't Insert Data")
        });
    })
})


//Delete Biodata
app.get("/history_delete/(:id)", (req, res) => {
    History.destroy({
        where: { id: req.params.id }
    }).then((game) => {
        
        History.findAll().then((game) => {
            res.render('history/delete', {
                history,
            })
        })
    });
});

app.get("/history_update/:id", (req, res) => {
    History.findAll({
        where: { id: req.params.id }
    }).then((history) => {
        res.render('history/update',{ 
            history
        }).catch((err) => {
            res.status(422).json("Can't Render Data")
        });
    });
});


//Update Biodata
app.post("/history-update/:id", (req,res) => {
    History.update({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        telp: req.body.telp,
        city: req.body.city,
        country: req.body.country,
        instagram: req.body.instagram,
    },
    {
        where: { id: req.params.id},
    }
    ).then((history) => {
        History.findAll().then((history) => {
            res.render('history', {
                history,
            })
        })
    }).catch((err) => {
        res.status(422).json("Can't Update Data")
    });
});

app.listen(port, () => {
    console.log("Server Running at Port 3000");
});