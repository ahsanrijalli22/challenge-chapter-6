
1. Buat Folder Challenge 
2. selanjutnya Install Sequelize
3. npm install sequelize-cli -g
4. npm init -y
5 .npm install sequelize
6. sequelize init → Auto tambah 4 folder , lalu config untuk db
7. npm install pg
8. sequelize db:create
9. sequelize model:generate —name user_game —attributes username:string,password:string 
   sequelize model:generate —name biodata —attributes firstname:string,lastname:string,email:string,city:string,country:string,telp:string,instagram:string
   sequelize model:generate —name history —attributes username:string,score:string,level:string,round:INTEGER,time:DATE
10 sequelize db:migrate
---
11. selanjutnya membuat table dengan sql shell
12. masuk mengunakan user dan database db_game dengan perintah disql shell 'postgres=# \c db_game;'
13. membuat table user_game dengan PK user_id yang dapat direlasikan dengan table lainnya dengan perintah
    " CREATE TABLE user_game (
	user_id serial PRIMARY KEY,
	username VARCHAR ( 50 ) UNIQUE NOT NULL,
	password VARCHAR ( 50 ) NOT NULL
    ); "
14. membuat table biodata_game yang mana id_user sebagai FOREIGN KEY yang mana dapat berelasi dengan tabel user game, perintah membuat tabel sebagai berikut:
    " CREATE TABLE user_biodata ( id_biodata SERIAL PRIMARY KEY, user_id INT NOT NULL, 
    firstname VARCHAR (50) ,lastname VARCHAR (50), email VARCHAR (50), city VARCHAR (50), country VARCHAR (50), 
    telp VARCHAR (15), instagram VARCHAR (25),FOREIGN KEY(user_id) REFERENCES user_game(user_id) ); "
15. membuat table history perintah query sebagai berikut
    CREATE TABLE user_history ( id_history SERIAL PRIMARY KEY, user_id INT NOT NULL, username VARCHAR (50) 
    ,score INT , level VARCHAR (25), round INT, time TIME, FOREIGN KEY(user_id) REFERENCES user_game(user_id) );

16. selanjutnya menginstal express dan ejs untuk routing dan pembuatan server.
17. membuat tampilan dashboard user game, biodata dan history dengan framework bootstrap yang mana diletakan di folder view untuk dasboard dan assets untuk css di foldel publik
18. membuat api untuk get data dan post halaman user, biodata dan history 
19. testing api di postman dan testing berhasil untuk get, post, put dan delete dengan output yang diinginkan.
20. SELESAI :)